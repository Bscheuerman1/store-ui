package finalprogram.bscheuerman1;

import java.util.Scanner;

public class PayAheadCustomer extends Customer {

    private double balance;

    PayAheadCustomer() {
        super();
        balance = 0;
        this.setStorage(1);
    }

    PayAheadCustomer(String newName, double newBalance) {
        this.setUsername(newName);
        this.setStorage(1);
        balance = newBalance;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    @Override
    public boolean checkOut(double total, double shippingCost, double fileSize) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Your total is : $" + total + " with extended costs of $" + shippingCost + " for shipping. Total space required for your files will be " + fileSize + "GB");
        System.out.println("Are you sure you want to check out with the given total and combined extra fees and storage space?");
        String answer = scan.nextLine();
        if (answer.equals("yes") || answer.equals("Yes")) {
            if (balance < total) { // Checks to see if the Pay-Ahead customer has enough to check-out
                while (balance < total) { // In the event that they do not, it will ask them if they would like to add more money to their account
                    System.out.println("You have insufficient funds in your account, would you like to add more?");
                    answer = scan.nextLine();
                    if (answer.equals("yes") || answer.equals("Yes")) {
                        System.out.println("Please enter the amount you would like to deposit"); // Will allow the customer to add money to their account
                        int amount = scan.nextInt();
                        balance = balance + amount;
                    } else { // If they refuse to, it will return them to the menu and decline their opportunity to check-out
                        System.out.println("Returning to menu..");
                        return false;
                    }
                }
            }
            System.out.println("Your items have been checked out!");
            setStorage(getStorage() - fileSize);
            // If they do have enough funds, they will be allowed to check out
            balance = balance - total; // the total balance will be subtracted by the total
            return true;
        } else { // Otherwise - they will return to the menu
            System.out.println("Returning to menu..");
        }

        return false;
    }
    @Override
    public void increase(double newBalance){
        balance = balance + newBalance;
    }

    public String toString() {
        return "User: " + getUsername() + "- Storage: " + getStorage() + "GB Balance: " + balance;
    }

}
