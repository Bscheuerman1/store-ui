/* This is the parent class for all customers, it contains the base for Regular Customer and basics that each
other customer class requires */
package finalprogram.bscheuerman1;

import java.util.ArrayList;
import java.util.Scanner;

public class Customer {

    private String username;
    private double storage;
    ArrayList<VirtualItem> virtualItems = new ArrayList<>();
    ArrayList<Item> shoppingCart = new ArrayList<>();

    public Customer() {
        username = "John Doe";
        storage = 5;
    }

    public Customer(String newName) {
        this();
        username = newName;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public double getStorage() {
        return storage;
    }

    public void setStorage(double storage) {
        this.storage = storage;
    }

    public ArrayList<VirtualItem> getVirtualItems() {
        return virtualItems;
    }

    public void addVirtualItems(VirtualItem newItem) {
        virtualItems.add(newItem);
        storage = storage - newItem.getFileSize();
    }

    public ArrayList<Item> getShoppingCart() {
        return shoppingCart;
    }

    public void addToCart(Item newItem) {
        shoppingCart.add(newItem);
    }

    public void removeItem() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter which item you would like to remove using numbers starting at 1");
        int index = scan.nextInt();
        shoppingCart.remove(index);
    }

    public void emptyCart() {
        shoppingCart.clear();
    }

    public void viewCart(double total, double shippingCost, double fileSize) {
        Scanner scan = new Scanner(System.in);
        System.out.println(shoppingCart);
        System.out.println("The total for your cart is: $" + total);
        System.out.println("Extended costs for shipping are: $" + shippingCost);
        System.out.println("Required room for virtual files is: " + fileSize + "GB");
        System.out.println("Would you like to remove an item from your cart?");
        String answer = scan.nextLine();
        if (answer.equals("yes") || answer.equals("Yes")) {
        removeItem();
        }
    }

    public void increase(double newBalance) {

    }

    public boolean checkOut(double total, double shippingCost, double fileSize) {
        Scanner scan = new Scanner(System.in);
        System.out.println("Your total is : $" + total + " with extended costs of $" + shippingCost + " for shipping. Total space required for your files will be " + fileSize + "GB");
        System.out.println("Are you sure you want to check out with the given total and combined extra fees and storage space?");
        String answer = scan.nextLine();
        if (answer.equals("yes") || answer.equals("Yes")) {
            System.out.println("Your items have been checked out!");
            storage = storage - fileSize;
            return true;
        } else {
            System.out.println("Returning to menu..");
        }
        return false;
    }

    public String toString() {
        return "User: " + getUsername() + "- Storage: " + getStorage() + "GB";
    }

}
