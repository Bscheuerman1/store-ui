package finalprogram.bscheuerman1;

import java.util.ArrayList;

public class Item {

    private double price;
    private String name;
    private int howMany;
    Item() {
        price = 10;
        name = "Lamp";
        howMany = 0;
    }

    Item(String newName, double newPrice) {
        name = newName;
        price = newPrice;
    }
    
    
    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHowMany() {
        return howMany;
    }

    public void setHowMany(int howMany) {
        this.howMany = howMany;
    }

    public double extra(){ // This method is to display the extra fee / storage associated with each item
        return 0;
    }
    
    public String toString() {
       
        return name + " - Price : $" + price;
    }

}
