package finalprogram.bscheuerman1;

import java.util.Scanner;

public class Store {

    // store they choose an item that gets added to customers cart.
    // if they check out later the item might go to the customers virtual items
    // go through the customers cart and use those items
    double total, shippingCost, totalFileSize;
    Customer customer = new Customer();
    Item itemList[] = new Item[10];
    
    public Store(){
        total = 0;
        shippingCost = 0;
        totalFileSize = 0;
    }

    public void addItem(int itemIndex) {
    // I will be decrementing itemIndex by 1 because the displayed index starts at 1, not 0
        boolean enoughSpace = false;
        /* Before adding an item, we check to see if it is a virtual item, if it
        is - does adding it to our current space surpass our limit? if so, it 
        will not allow the user to get the item.
        */
        if(itemList[itemIndex - 1] instanceof VirtualItem){
            if((itemList[itemIndex - 1].extra() + totalFileSize) < customer.getStorage()){
            enoughSpace = true; // If this becomes true, the user has enough space to buy their virtual item
        }
        }
        if(itemList[itemIndex - 1] instanceof PhysicalItem){ // Always allow physical items to pass. They have no restrictions.
            // If a payAhead customer does not have enough balance, it will be checked in checkout. This will allow them to still buy their items, if they add money. 
            enoughSpace = true;
        } 
        
        if(enoughSpace){
        Scanner scanner = new Scanner(System.in);
        System.out.println("How many would you like to add?"); // Allows the customer to buy multiples of one item
        int userInput = scanner.nextInt();
        if (itemIndex > 0 && itemIndex <= 10) { // As long as the item is within range...they can only buy up to 10.
            for (int i = 0; i < userInput; i++) { // This will loop for the amount of items that they wish to buy. That way the item may be added multiple times.
                 if(itemList[itemIndex - 1] instanceof VirtualItem)
                {  
                    totalFileSize = totalFileSize + itemList[itemIndex - 1].extra(); // Is this a virtual item? if so, increase the total file size.
                }
                  if(itemList[itemIndex - 1] instanceof PhysicalItem) // Is this a physical item?
                  {
                    // Shipping cost is recorded using the shipping cost from the item.
                    shippingCost = shippingCost + itemList[itemIndex - 1].extra();
                    if(customer instanceof LoyalCustomer){
                        shippingCost = 0;
                }
                }
                customer.addToCart(itemList[itemIndex - 1]);
                itemList[itemIndex - 1].setHowMany(userInput);
            }
        }
        else{
            System.out.println("You have chosen to add an insuffecient amount of items. It has not been added...");
        }
        }
        else{
            System.out.println("You do not have enough storage space for this item...");
        }
        collectTotal(); // Total is recorded every time a new item is added
    }

    public void displayCart() {
        getTotal();
        customer.viewCart(total, shippingCost, totalFileSize);
    }
    public void clearCart() {
        customer.emptyCart();      
        this.total = 0;
        this.shippingCost = 0;
        this.totalFileSize = 0;
        for(int i = 0; i < itemList.length; i++){
            itemList[i].setHowMany(0);
        }
        System.out.println(customer.getUsername() + "- Your cart has been cleared!");
     
    }

    public boolean add(Item newItem) {
        for (int i = 0; i < itemList.length; i++) {
            if (itemList[i] == null) {
                itemList[i] = newItem;
                return true;
            }
        }
        return false;
    }
    
 
     
    public void customerCheckOut(){
        if((customer.checkOut(total, shippingCost, totalFileSize))){
        addVirtualItems();
        clearCart();
        }
    }
    
    public void displayVirtualItems(){
        System.out.println(customer.getVirtualItems());
    }
    
    public void addVirtualItems(){
        for(int i = 0; i < 5; i++){ // The first 5 items are virtual items, this will go through to see which have been purchased & add them to the virtualList
            if(itemList[i].getHowMany() > 0){
                for(int j = 0; j < itemList[i].getHowMany(); j++){
                customer.addVirtualItems((VirtualItem)itemList[i]);
                }
            }
        }
    }
    public double collectTotal() {
        for (int i = 0; i < itemList.length; i++) {
            total = total + (itemList[i].getPrice() * itemList[i].getHowMany()); // Based on the quantity of the current item, it will add that many to the total.
        }
        return total;
    }
    public double getTotal(){
        return total;
    }
    
    public void setTotal(double total) {
        this.total = total;
    }

    public double getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(double shippingCost) {
        this.shippingCost = shippingCost;
    }

    public double getFileSize() {
        return totalFileSize;
    }

    public void setTotalFileSize(double newFileSize) {
        this.totalFileSize = newFileSize;
    }
    public void setCustomer(Customer newCustomer){
        customer = newCustomer; // This sets customer to the current customer that is shopping
        total = 0;
        shippingCost = 0;
        totalFileSize = 0;
    }

    public String toString() {
        String sentence = "";
        for (int i = 0; i < itemList.length; i++) {
            sentence = sentence + (i + 1) + ") " + itemList[i] + "\n";
        }
        return sentence;
    }
    

}
