
package finalprogram.bscheuerman1;

public class PhysicalItem extends Item{
    private double shippingCost;
    
    PhysicalItem(){
        super();
        this.setName("Lamp");
        this.setPrice(10);
        this.shippingCost = 3.50;
    }
PhysicalItem(String newName, double newPrice, double newShipCost){
        setName(newName);
        setPrice(newPrice);
        this.shippingCost = newShipCost;
    }
    public double getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(double shippingCost) {
        this.shippingCost = shippingCost;
    }
    
    public double extra(){
       return shippingCost;
    }
    
    
    public String toString(){
        return getName() + " - Price : $" + getPrice() + " Price for Shipping : $" + shippingCost; 
    }
    
}
