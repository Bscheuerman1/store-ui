
package finalprogram.bscheuerman1;

import java.util.Scanner;


public class LoyalCustomer extends Customer {
    
    LoyalCustomer(){
        super();
        this.setStorage(10);
        
    }
    LoyalCustomer(String newName){
        this.setStorage(10);
        setUsername(newName);
    }
    
    // The follow methods are exactly the same as the parent class except the loyal customer recieved a 5% discount from the total.
    
    @Override
    public boolean checkOut(double total, double shippingCost, double fileSize){
         Scanner scan = new Scanner(System.in);
         double discount = total * .05;
         total = total - discount;
         System.out.println("Your total is : $" + total + " with extended costs of $" + shippingCost + " for shipping. Total space required for your files will be " + fileSize + "GB");
         System.out.println("Are you sure you want to check out with the given total and combined extra fees and storage space?");
         String answer = scan.nextLine();
         if(answer.equals("yes") || answer.equals("Yes")){
             System.out.println("Your items have been checked out!");
             setStorage(getStorage() - fileSize);
             return true;
         }
         else
         {
             System.out.println("Returning to menu..");
         }
         return false;
        
        
        }
    
    @Override
    public void viewCart(double total, double shippingCost, double fileSize) {
        double discount = total * .05;
        total = total - discount;
        Scanner scan = new Scanner(System.in);
        System.out.println(shoppingCart);
        System.out.println("The total for your cart is: $" + total);
        System.out.println("Extended costs for shipping are: $" + shippingCost);
        System.out.println("Required room for virtual files is: " + fileSize + "GB");
        System.out.println("Would you like to remove an item from your cart?");
        String answer = scan.nextLine();
        if (answer.equals("yes") || answer.equals("Yes")) {
        removeItem();
        }
    }
    
    public String toString(){
        return "User: " + getUsername() + "- Storage: " + getStorage() + "GB"; 
    }
}
