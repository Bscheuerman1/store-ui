package finalprogram.bscheuerman1;

import java.util.ArrayList;
import java.util.Scanner;

public class FinalprogramBscheuerman1 {
// The usernames of the customers are as follows -
    // Jane Doe - A regular customer
    // Rick Ross - A pay-ahead customer
    // Bob Smith - A loyal customer
    
    // any questions that ask for answers without ordered numbers shown or prompted can be answered as "yes" or "Yes"
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        Store store = new Store();
        Customer[] user = new Customer[3];
        // Creating new items
        VirtualItem salsa = new VirtualItem("Salsa.mp3", 2.99, .13);
        VirtualItem howto = new VirtualItem("How_to.txt", 10.99, .54);
        VirtualItem wow = new VirtualItem("WOW.mp4", 25.44, 2.17);
        VirtualItem virus = new VirtualItem("Virus.exe", 59.99, 4.42);
        VirtualItem mystery = new VirtualItem("Mystery.zip", 12.50, 2.11);
        PhysicalItem table = new PhysicalItem("Table", 35, 9.50);
        PhysicalItem lamp = new PhysicalItem("Lamp", 12.99, 3);
        PhysicalItem bed = new PhysicalItem("Bed", 250.99, 20);
        PhysicalItem couch = new PhysicalItem("Couch", 100, 15);
        PhysicalItem stool = new PhysicalItem("Stool", 21.50, 5);

        // Adding the new items to the store
        store.add(salsa);
        store.add(howto);
        store.add(wow);
        store.add(virus);
        store.add(mystery);
        store.add(table);
        store.add(lamp);
        store.add(bed);
        store.add(couch);
        store.add(stool);
        // Creating new customers
        user[0] = new Customer("Jane Doe");
        user[1] = new PayAheadCustomer("Rick Ross", 0);
        user[2] = new LoyalCustomer("Bob Smith");
        // Giving the new customers virtual items that they already own
        user[0].addVirtualItems(salsa);
        user[0].addVirtualItems(howto);
        user[0].addVirtualItems(wow);
        user[1].addVirtualItems(howto);
        user[2].addVirtualItems(virus);
        user[2].addVirtualItems(mystery);
        user[2].addVirtualItems(wow);

        // USER INTERFACE BEGINS HERE 
        // ---------------------------------------------------------
        int userID = login();
        /* This will be the type of customer based on who logs in.
           This calls the login method which prompts the user to log in or Exit */
        while (userID != 0 && userID != 1 && userID != 2 && userID != 3) {
            System.out.println("Account does not exist, or input was misunderstood. Please try again");
            userID = login();
        }
        if (userID < 3) {
            store.setCustomer(user[userID]);     // the store sets the customer shopping to whatever user is currently logged in
            // We use an if statement to avoid corrupting the array
        }
        while (userID == 0 || userID == 1 || userID == 2) {
            int choice = options(userID);
            if (choice > 0 && choice < 8) {
                if (choice == 1) {
                    userID = login();
                    while (userID != 0 && userID != 1 && userID != 2 && userID != 3) {
                        System.out.println("Account does not exist, or input was misunderstood. Please try again");
                        userID = login();
                    }
                    if (userID < 3) {
                        store.setCustomer(user[userID]);
                    }
                }
                if (choice == 2) {
                    store.displayVirtualItems();
                }
                if (choice == 3) {
                    System.out.println(store);
                    int buy = buy();
                    if (buy > 0 && buy < 11) {
                        store.addItem(buy);
                    }
                }
                if (choice == 4) {
                    store.displayCart(); // This will display the current cart
                }
                if (choice == 5) {
                    store.clearCart(); // This allows the customer to completely clean out their cart
                }
                if (choice == 6) {
                    store.customerCheckOut(); // Prompts the customer to check out
                }
                if (choice == 7) {
                    if (userID == 1) { // IF the user is a LoyalCustomer it will allow them to deposit money
                        System.out.println("How much money would you like to deposit into your account?");
                        double amount = scan.nextDouble();
                        user[1].increase(amount);
                    }
                }
            }
        }

        System.out.println("The program has ended");

    }

    public static int login() {
        /* This method allows the current user to log in as a customer,
        when doing so, it will check the current log in infromation with 
        existing users, and if it is a match it will return the number assocaited
        with their location in the array. If it does not match, it will return 
        the number entered and the interface will deal with it from there.
        */
        Scanner scan = new Scanner(System.in);
        System.out.println("Please enter in your username to log in, or EXIT to exit");
        String userName = scan.nextLine();
        if (userName.equals("Jane Doe")) {
            return 0;
        }
        if (userName.equals("Rick Ross")) {
            return 1;
        }
        if (userName.equals("Bob Smith")) {
            return 2;
        }
        if (userName.equals("EXIT") || userName.equals("exit")) {
            return 3;
        }
        return 4;
    }

    public static int options(int index) {
     // This method displays the available options for the current customer to choose from
        System.out.println("Please choose from the following options: ");
        System.out.println("1) Log out");
        System.out.println("2) View owned virtual items ");
        System.out.println("3) Browse the store");
        System.out.println("4) View cart");
        System.out.println("5) Empty cart");
        System.out.println("6) Check out");
        if (index == 1) {
            System.out.println("7) Deposit money to your balance");
        }
        Scanner scan = new Scanner(System.in);
        System.out.println("Which of the following options would you like to carry out?");
        int answer = scan.nextInt();
        return answer; // Returns the option that they woud like to carry out
    }

    public static int buy() {
        // This method allows the customer to choose which item they would like to buy from the store
        Scanner scan = new Scanner(System.in);
        System.out.println("What item would you like to buy? Please use the numbers shown");
        int answer = scan.nextInt();
        return answer;
    }
}
