
package finalprogram.bscheuerman1;

public class VirtualItem extends Item{
    private double fileSize;
    
    VirtualItem(){
        super();
        this.setName("New Groove.mp3");
        this.setPrice(.99);
        this.fileSize = .63;
    }
    VirtualItem(String newName, double newPrice, double newFileSize){
        setName(newName);
        setPrice(newPrice);
        this.fileSize = newFileSize;
    }

    public double getFileSize() {
        return fileSize;
    }

    public void setFileSize(double fileSize) {
        this.fileSize = fileSize;
    }
    
    public double extra(){
        return fileSize;
    }
    
    public String toString(){
        return getName() + " - Price : $" + getPrice() + " Size : " + fileSize + "GB"; 
    }
}
